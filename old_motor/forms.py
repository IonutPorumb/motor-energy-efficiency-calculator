from django import forms
from django.forms import TextInput, Select

from old_motor.models import OldMotor


class OldMotorForm(forms.ModelForm):
    class Meta:
        model = OldMotor
        fields = ['application_name', 'motor_producer', 'motor_order_code',
                  'rated_voltageY', 'rated_voltageD1', 'rated_voltageD2',
                  'rated_frequencyY', 'rated_frequencyD1', 'rated_frequencyD2',
                  'rated_power_Y', 'rated_power_D1', 'rated_power_D2',
                  'rated_speed_50HzY', 'rated_speed_50HzD1', 'rated_speed_60HzD2',
                  'rated_current_50Hz_Y', 'rated_current_50Hz_D1', 'rated_current_60Hz_D2',
                  'power_factor_50HzY', 'power_factor_50HzD1', 'power_factor_60HzD2',
                  'type_of_construction', 'frame_size',
                  'enclosure_type', 'degree_of_protection']
        widgets = {
            'application_name': TextInput(
                attrs={'placeholder': 'Insert the application name', 'class': 'form-control', 'style': 'opacity:70%;'}),
            'motor_producer': TextInput(attrs={'placeholder': 'Insert the motor producer', 'class': 'form-control',
                                               'style': 'opacity:70%;'}),
            'motor_order_code': TextInput(
                attrs={'place-holder': 'Insert the motor order code', 'class': 'form-control',
                       'style': 'opacity:70%;'}),
            'rated_voltageY': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_voltageD1': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_voltageD2': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            # 'rated_voltageD3': Select(attrs={'rows': '3', 'cols': '5'}),
            'rated_frequencyY': TextInput(attrs={'place-holder': 'Insert the rated frequency for Y connection',
                                                 'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_frequencyD1': TextInput(attrs={'place-holder': 'Insert the rated frequency for D1 connection',
                                                  'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_frequencyD2': TextInput(attrs={'place-holder': 'Insert the rated frequency for D2 connection',
                                                  'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_power_Y': TextInput(attrs={'place-holder': 'Insert the rated power for Y connection',
                                              'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_power_D1': TextInput(attrs={'place-holder': 'Insert the rated power for D1 connection',
                                               'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_power_D2': TextInput(attrs={'place-holder': 'Insert the rated power for D2 connection',
                                               'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_speed_50HzY': TextInput(attrs={'place-holder': 'Insert the rated speed at 50 Hz and Y connection',
                                                  'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_speed_50HzD1': TextInput(attrs={'place-holder': 'Insert the rated speed at 50 Hz and D1 connection',
                                                   'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_speed_60HzD2': TextInput(attrs={'place-holder': 'Insert the rated speed at 60 Hz and D2 connection',
                                                   'class': 'form-control', 'style': 'opacity:70%;'}),
            # 'rated_speed_50HzD3': TextInput(attrs={'rows': '3', 'cols': '5'}),
            'rated_current_50Hz_Y': TextInput(attrs={'place-holder': 'Introduce the rated current at 50 Hz and Y '
                                                                     'connection', 'class': 'form-control',
                                                     'style': 'opacity:70%;'}),
            'rated_current_50Hz_D1': TextInput(attrs={'place-holder': 'Introduce the rated current at 50 Hz and D1 '
                                                                      'connection', 'class': 'form-control',
                                                      'style': 'opacity:70%;'}),
            'rated_current_60Hz_D2': TextInput(attrs={'place-holder': 'Introduce the rated current at 60 Hz and D2 '
                                                                      'connection', 'class': 'form-control',
                                                      'style': 'opacity:70%;'}),
            # 'rated_current_50Hz_D3': TextInput(attrs={'rows': '3', 'cols': '5'}),
            'power_factor_50HzY': TextInput(attrs={'place-holder': 'Introduce the rated power factor at 50 Hz and '
                                                                   'Y connection', 'class': 'form-control',
                                                   'style': 'opacity:70%;'}),
            'power_factor_50HzD1': TextInput(attrs={'place-holder': 'Introduce the rated power factor at 50 Hz and '
                                                                    'D1 connection', 'class': 'form-control',
                                                    'style': 'opacity:70%;'}),
            'power_factor_60HzD2': TextInput(attrs={'place-holder': 'Introduce the rated power factor at 60 Hz and '
                                                                    'D2 connection', 'class': 'form-control',
                                                    'style': 'opacity:70%;'}),
            'type_of_construction': Select(attrs={'place-holder': 'Insert the motor type of construction',
                                                  'class': 'form-control', 'style': 'opacity:70%;'}),
            'frame_size': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'enclosure_type': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'degree_of_protection': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
        }

    def __init__(self, *args, **kwargs):
        super(OldMotorForm, self).__init__(*args, **kwargs)
