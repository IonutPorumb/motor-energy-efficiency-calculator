from django.db import models

import new_motor.models


class OldMotor(models.Model):
    voltage_options = ((220, 220), (230, 230),
                       (240, 240), (380, 380),
                       (400, 400), (415, 415),
                       (440, 440), (460, 460),
                       (480, 480), (500, 500),
                       (575, 575), (660, 660),
                       (690, 690))
    construction_options = (('IM B3', 'IM B3'),
                            ('IM B5', 'IM B5'),
                            ('IM B35', 'IM B35'),
                            ('IM B14', 'IM B14'))
    enclosure_options = (('Aluminium', 'Aluminium'), ('Cast iron', 'Cast iron'))
    degree_of_protection_options = (('IP54', 'IP54'), ('IP55', 'IP55'), ('IP56', 'IP56'),
                                    ('IP65', 'IP65'), ('IP66', 'IP66'))
    application_name = models.TextField(max_length=100, default='Motor')
    motor_producer = models.TextField(max_length=100, null=True, blank=True)
    motor_order_code = models.TextField(max_length=20, null=True, blank=True)
    rated_voltageY = models.IntegerField(choices=voltage_options, null=True)
    rated_voltageD1 = models.IntegerField(choices=voltage_options, null=True)
    rated_voltageD2 = models.IntegerField(choices=voltage_options, null=True)
    rated_frequencyY = models.IntegerField(default=50)
    rated_frequencyD1 = models.IntegerField(default=50)
    rated_frequencyD2 = models.IntegerField(default=60)
    rated_power_Y = models.FloatField(null=True)
    rated_power_D1 = models.FloatField(null=True)
    rated_power_D2 = models.FloatField(null=True)
    rated_speed_50HzY = models.IntegerField(null=True)
    rated_speed_50HzD1 = models.IntegerField(null=True)
    rated_speed_60HzD2 = models.IntegerField(null=True)
    pole_no = models.IntegerField(null=True)
    rated_current_50Hz_Y = models.FloatField(null=True)
    rated_current_50Hz_D1 = models.FloatField(null=True)
    rated_current_60Hz_D2 = models.FloatField(null=True)
    power_factor_50HzY = models.FloatField(null=True)
    power_factor_50HzD1 = models.FloatField(null=True)
    power_factor_60HzD2 = models.FloatField(null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)
    type_of_construction = models.CharField(max_length=6, choices=construction_options)
    frame_size = models.CharField(max_length=4, choices=new_motor.models.NewMotor.frame_size_options)
    enclosure_type = models.CharField(max_length=10, null=True, blank=True, choices=enclosure_options)
    degree_of_protection = models.CharField(max_length=4, null=True, blank=True, choices=degree_of_protection_options)

    def __str__(self):
        return f"Rated voltage Y: {self.rated_voltageY}, Rated voltage D: {self.rated_voltageD1}," \
               f"Rated power: {self.rated_power_D1}, Rated speed: {self.rated_speed_50HzD1}"
