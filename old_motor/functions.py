from math import sqrt


# Efficiency calculator
from old_motor.models import OldMotor


def efficiency_calculator(power, current, voltage, power_factor):
    print(power, current, voltage, power_factor)
    print(type(power), type(current), type(voltage), type(power_factor))
    power_e = sqrt(3) * int(voltage) * current * power_factor
    efficiency = format((power * 1000 * 100 / power_e), ".2f")
    return efficiency


# Energy calculator
def energy_calculator(current, voltage, power_factor, op_hours):
    power_e = sqrt(3) * int(voltage) * current * power_factor
    energy = format((power_e * op_hours/1000), ".2f")
    return energy


if __name__ == "__main__":
    print(efficiency_calculator(3, 5.7, 400, 0.86))
    print(energy_calculator(5.7, 400, 0.86, 10))



