
from django.urls import path

from old_motor import views

urlpatterns = [
    path('create-old-motor/', views.OldMotorCreateView.as_view(), name='create_old_motor'),
    # path('list-of-old-motors/', views.OldMotorListView.as_view(), name='view_list_old_motors'),
    path('list-of-old-motors/', views.oldMotorView, name='view_list_old_motors'),
    path('update-old-motor/<int:pk>', views.OldMotorUpdateView.as_view(), name='update_old_motor'),
    path('delete-old-motor/<int:pk>/', views.OldMotorDeleteView.as_view(), name='delete_old_motor'),
    path('detail-old-motor/<int:pk>/', views.OldMotorDetailView.as_view(), name='detail_old_motor'),
    path('generate_pdf_old_motor/<int:pk>/', views.generate_pdf_per_motor, name='generate-pdf-old-motor'),

]
