from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.forms import Form
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from old_motor import functions
from old_motor.forms import OldMotorForm
from old_motor.models import OldMotor
from savings_and_commercial_data.functions import show_savings_and_commercial_data
from savings_and_commercial_data.models import SavingsAndCommercialData
from savings_and_commercial_data.views import render_to_pdf


class OldMotorCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'old_motor/old_motor_home.html'
    # form_class = OldMotorForm
    permission_required = 'old_motor.add_old_motor'
    fields = '__all__'
    model = OldMotor

    # success_url = reverse_lazy('show_alternative_new_motor', kwargs={'pk': object.id})

    def get_success_url(self):
        return f"/show-alternative-new-motor/{self.object.id}/"

    def post(self, request, *args, **kwargs):
        print(self.request.POST)
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        print('form_valid')
        valid = super().form_valid(form)
        print(valid)
        return valid

    def form_invalid(self, form):
        print('form_invalid')
        valid = super().form_invalid(form)
        print(valid)
        form: Form = form
        print(form.errors)
        return valid


# Show date form DB


class OldMotorListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'old_motor/list_old_motor.html'
    model = OldMotor
    permission_required = 'old_motor.view_list_old_motors'
    context_object_name = 'all_existing_motors'


# Testing if the old motor list view work's as a function
@login_required
def oldMotorView(request):
    old_motor_list = OldMotor.objects.all()
    savings_and_commercial_data_list = SavingsAndCommercialData.objects.all()
    registration_exist = -1     # it means there is no registration in the table DB
    motor_id_list = []

    for old_motor in old_motor_list:
        try:
            print("*************", SavingsAndCommercialData.objects.get(old_motor_id_savings=old_motor.id))
            registration_exist = 1
            print("++++++++++++", registration_exist)
        except ObjectDoesNotExist:
            print("The object with the id:", old_motor.id, ", does not exist!")
            registration_exist = -1
            motor_id_list.append(old_motor.id)
            print("++++++++++++++++==", motor_id_list)
            print("++++++++++++", registration_exist)
            print("++++++++++++", motor_id_list)

    return render(request, '../templates/old_motor/list_old_motor.html',
                  {'all_existing_motors': old_motor_list,
                   'savings_and_commercial_data_list': savings_and_commercial_data_list,
                   'motor_id_list': motor_id_list,
                   'registration_exist': registration_exist,
                   })


# Update old motor form List
class OldMotorUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'old_motor/update_old_motor.html'
    model = OldMotor
    permission_required = 'old_motor.change_old_motor'
    form_class = OldMotorForm
    success_url = reverse_lazy('view_list_old_motors')


# Delete old motor from List
class OldMotorDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'old_motor/delete_old_motor.html'
    model = OldMotor
    permission_required = 'old_motor.delete_old_motor'
    success_url = reverse_lazy('view_list_old_motors')
    context_object_name = 'old_motor'


class OldMotorDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'old_motor/detail_old_motor.html'
    model = OldMotor
    permission_required = 'old_motor.view_old_motor'
    context_object_name = 'old_motor'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        motor: OldMotor = context[self.context_object_name]
        context['efficiency'] = functions.efficiency_calculator(motor.rated_power_D1, motor.rated_current_50Hz_D1,
                                                                motor.rated_voltageD1, motor.power_factor_50HzD1)
        return context


def generate_pdf_per_motor(request, pk):
    old_motor = OldMotor.objects.get(id=pk)
    context = {
        'old_motor_details': old_motor
    }
    pdf_file = render_to_pdf('old_motor/one_old_motor.html', context)
    if pdf_file:
        response = HttpResponse(pdf_file, content_type='application/pdf')
        filename = f'Details about the old motor {old_motor.motor_producer} {old_motor.rated_power_D1}'
        content = 'inline; filename=%s' % filename
        response['Content-Disposition'] = content
        return response
