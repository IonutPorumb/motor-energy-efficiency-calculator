from django.apps import AppConfig


class ExistingMotorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'old_motor'
