from django.db import models


class SavingsAndCommercialData(models.Model):

    energy_price = models.DecimalField(max_digits=4, decimal_places=3, null=True)
    operating_hours_day = models.IntegerField(null=True, blank=True)
    operating_days_year = models.IntegerField(null=True, blank=True)
    operating_hours_year = models.IntegerField(null=True, blank=True)
    live_time_years = models.IntegerField(null=True, blank=True)
    new_motor_energy_year = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    old_motor_energy_year = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    new_motor_energy_price_year = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    old_motor_energy_price_year = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    # new_motor_energy_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    new_motor_total_costs_in_live_time = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    old_motor_total_costs_in_live_time = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    live_time_new_motor_energy_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    live_time_new_motor_energy = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    live_time_old_motor_energy_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    live_time_old_motor_energy = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    saved_energy_live_time = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    customer_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    roi_time_years = models.DecimalField(max_digits=3, decimal_places=1, null=True)
    saved_money_live_time = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    price_discount = models.IntegerField(null=True, blank=True)
    old_motor_maintenance = models.IntegerField(null=True, blank=True)
    co2_savings_life_time = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    old_motor_id_savings = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f"Old motor ID savings: {self.old_motor_id_savings}\n"\
               f"Energy price: {self.energy_price}\n" \
               f"Operating hours/year: {self.operating_hours_year}\n" \
               f"Live time: {self.live_time_years}\n" \
               f"New motor energy consumption/year: {self.new_motor_energy_price_year}\n" \
               f"Old motor energy consumption/year: {self.old_motor_energy_price_year}\n" \
               f"New motor energy price/year: {self.new_motor_energy_price_year}\n" \
               f"Old motor energy price/year: {self.old_motor_energy_price_year}\n" \
               f"Saved energy: {self.saved_energy_live_time}\n" \
               f"Saved money: {self.saved_money_live_time}\n" \
               f"Return of investment/years: {self.roi_time_years}\n" \
               f"Saved money: {self.saved_money_live_time}"
