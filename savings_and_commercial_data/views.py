import math
from io import BytesIO

from xhtml2pdf import pisa
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.template.loader import get_template
from django.shortcuts import render
from new_motor.functions import chose_new_motor
from new_motor.models import NewMotor
from old_motor.models import OldMotor
from savings_and_commercial_data.forms import SavingsAndCommercialDataForm
from savings_and_commercial_data.functions import energy_calculation_kWh_day, amortization_years, \
    show_savings_and_commercial_data
from savings_and_commercial_data.models import SavingsAndCommercialData


@login_required
def savings_view(request, pk):
    compatible_motors = chose_new_motor(pk)
    old_motor = OldMotor.objects.get(id=pk)
    context = {}
    form = SavingsAndCommercialDataForm()
    context['form'] = form  # trimitere formular nou in html
    co2_factor = 0.632
    new_motor_current = compatible_motors.get().rated_current_50Hz_D
    new_motor_voltage = compatible_motors.get().rated_voltage_50Hz_D
    new_motor_power_factor = compatible_motors.get().power_factor_50Hz_4_4
    new_motor_list_price = compatible_motors.get().motor_price
    old_motor_current = old_motor.rated_current_50Hz_D1
    old_motor_voltage = old_motor.rated_voltageD1
    old_motor_power_factor = old_motor.power_factor_50HzD1
    old_motor_id = old_motor.id
    if request.method == 'POST':
        form = SavingsAndCommercialDataForm(data=request.POST)
        if form.is_valid():
            energy_price_kWh = float(request.POST.get('energy_price', 0))  # valoarea citita din formular
            operating_hours_day = int(request.POST.get('operating_hours_day', 0))
            operating_days_year = int(request.POST.get('operating_days_year', 0))
            operating_hours_year = operating_days_year * operating_hours_day
            live_time_years = int(request.POST.get('live_time_years', 0))
            old_motor_maintenance_price = float(request.POST.get('old_motor_maintenance_price', 0))
            old_motor_energy_day = energy_calculation_kWh_day(old_motor_current, old_motor_voltage,
                                                              old_motor_power_factor, operating_hours_day)
            old_motor_energy_year = round(energy_calculation_kWh_day(old_motor_current, old_motor_voltage,
                                                                     old_motor_power_factor, operating_hours_year), 2)
            live_time_old_motor_energy = round(old_motor_energy_year * live_time_years, 2)
            old_motor_energy_price_day = old_motor_energy_day * energy_price_kWh
            old_motor_energy_price_year = round(old_motor_energy_year * energy_price_kWh, 2)
            live_time_old_motor_energy_price = round(old_motor_energy_price_year * live_time_years, 2)
            old_motor_total_costs_in_live_time = round(live_time_old_motor_energy_price +
                                                       old_motor_maintenance_price, 2)
            price_discount = int(request.POST.get('price_discount', 0))
            customer_price = round(new_motor_list_price * (1 - price_discount / 100), 2)
            new_motor_energy_day = energy_calculation_kWh_day(new_motor_current, new_motor_voltage,
                                                              new_motor_power_factor, operating_hours_day)
            new_motor_energy_year = energy_calculation_kWh_day(new_motor_current, new_motor_voltage,
                                                               new_motor_power_factor, operating_hours_year)
            live_time_new_motor_energy = round(new_motor_energy_year * live_time_years, 2)
            new_motor_energy_price_day = new_motor_energy_day * energy_price_kWh
            new_motor_energy_price_year = round(new_motor_energy_year * energy_price_kWh, 2)
            print("===============================================old_motor_energy_price_day",
                  old_motor_energy_price_day)
            print("===============================================new_motor_energy_price_day",
                  new_motor_energy_price_day)
            print("===============================================new_motor_energy_price_year",
                  new_motor_energy_price_year )
            print("===============================================old_motor_energy_price_year",
                  old_motor_energy_price_year)
            live_time_new_motor_energy_price = round(new_motor_energy_price_year * live_time_years, 2)
            new_motor_total_costs_in_live_time = round(live_time_new_motor_energy_price + customer_price, 2)
            saved_energy_year = old_motor_energy_year - new_motor_energy_year
            saved_energy_live_time = round(saved_energy_year * live_time_years, 2)
            print("===========", customer_price)
            print("+++++++++++++", price_discount)
            print(">>>>>>>>>>>", old_motor_maintenance_price)
            # saved_money_year = old_motor_energy_price_year - new_motor_energy_price_year

            roi_time_years = round(
                amortization_years(new_motor_energy_price_year, old_motor_energy_price_year, customer_price), 2)
            saved_money_live_time = round(old_motor_total_costs_in_live_time - new_motor_total_costs_in_live_time, 2)
            co2_savings_life_time = round(saved_energy_live_time * co2_factor, 2)
            context['operating_hours_year'] = operating_hours_year
            context['old_motor_energy_year'] = old_motor_energy_year
            context['live_time_years'] = live_time_years
            context['live_time_old_motor_energy'] = live_time_old_motor_energy
            context['old_motor_energy_price_year'] = old_motor_energy_price_year
            context['live_time_old_motor_energy_price'] = live_time_old_motor_energy_price
            context['old_motor_total_costs_in_live_time'] = old_motor_total_costs_in_live_time
            context['new_motor_energy_year'] = new_motor_energy_year
            context['live_time_new_motor_energy'] = live_time_new_motor_energy
            context['new_motor_energy_price_year'] = new_motor_energy_price_year
            context['live_time_new_motor_energy_price'] = live_time_new_motor_energy_price
            context['new_motor_list_price'] = new_motor_list_price
            context['customer_price'] = customer_price
            context['price_discount'] = price_discount
            context['new_motor_total_costs_in_live_time'] = new_motor_total_costs_in_live_time
            context['roi_time_years'] = roi_time_years
            context['saved_money_live_time'] = saved_money_live_time
            context['co2_savings_life_time'] = co2_savings_life_time
            context['saved_energy_live_time'] = saved_energy_live_time
            savings_and_commercial_data = form.save()
            savings_and_commercial_data.operating_hours_year = operating_hours_year
            savings_and_commercial_data.old_motor_energy_year = old_motor_energy_year
            savings_and_commercial_data.live_time_years = live_time_years
            savings_and_commercial_data.live_time_old_motor_energy = live_time_old_motor_energy
            savings_and_commercial_data.old_motor_energy_price_year = old_motor_energy_price_year
            savings_and_commercial_data.live_time_old_motor_energy_price = live_time_old_motor_energy_price
            savings_and_commercial_data.live_time_old_motor_energy_price = live_time_old_motor_energy_price
            savings_and_commercial_data.old_motor_total_costs_in_live_time = old_motor_total_costs_in_live_time
            savings_and_commercial_data.new_motor_energy_year = new_motor_energy_year
            savings_and_commercial_data.live_time_new_motor_energy = live_time_new_motor_energy
            savings_and_commercial_data.customer_price = customer_price
            savings_and_commercial_data.new_motor_total_costs_in_live_time = new_motor_total_costs_in_live_time
            savings_and_commercial_data.roi_time_years = roi_time_years
            savings_and_commercial_data.saved_money_live_time = saved_money_live_time
            savings_and_commercial_data.co2_savings_life_time = co2_savings_life_time
            savings_and_commercial_data.saved_energy_live_time = saved_energy_live_time
            savings_and_commercial_data.price_discount = price_discount
            savings_and_commercial_data.live_time_new_motor_energy_price = live_time_new_motor_energy_price
            savings_and_commercial_data.new_motor_energy_price_year = new_motor_energy_price_year
            savings_and_commercial_data.old_motor_id_savings = old_motor_id
            savings_and_commercial_data.old_motor_maintenance = old_motor_maintenance_price
            print("+++++++++++++++++++++Old motor ID is :", old_motor_id)

            savings_and_commercial_data.save()

    return render(request, '../templates/savings_and_commercial_data/create_savings_and_commercial_data.html', context)


# class SavingsAndCommercialDataCreateView(CreateView):
#     template_name = 'savings_and_commercial_data/create_savings_and_commercial_data.html'
#     form_class = SavingsAndCommercialDataForm
#     # fields = '__all__'
#     model = SavingsAndCommercialData


@login_required
def show_savings_and_commercial_data_view(request, pk):
    compatible_new_motor = chose_new_motor(pk)
    old_motor = OldMotor.objects.get(id=pk)
    compatible_savings_data = show_savings_and_commercial_data(pk)
    savings = SavingsAndCommercialData.objects.get(old_motor_id_savings=pk)

    old_motor_energy_price_year = float(savings.old_motor_energy_price_year)
    new_motor_energy_price_year = float(savings.new_motor_energy_price_year)
    old_motor_maintenance = float(savings.old_motor_maintenance)
    customer_price = float(savings.customer_price)
    x_var = math.ceil(savings.roi_time_years)  # 'X' end of scale
    print("++++++++============++++++++++++", x_var)
    years = float(0.0)

    maintenance_rate = float(savings.old_motor_maintenance)
    amortization_rate = float(savings.customer_price)
    old_motor_Y_value_0 = float(old_motor_energy_price_year * years + old_motor_maintenance/(10*(x_var+1)))
    new_motor_Y_value_0 = float(new_motor_energy_price_year * years + customer_price)

    data = [['Year', 'Old motor', 'New motor'],
            [0, old_motor_Y_value_0, new_motor_Y_value_0]]

    for i in range(0, 10*(x_var+1)+1):
        years += float(0.1)
        print("++++++++============++++++++++++", years)
        old_motor_Y_value = float(old_motor_energy_price_year * years + maintenance_rate/(10*(x_var+1)))
        new_motor_Y_value = float(new_motor_energy_price_year * years + amortization_rate)
        data.append([years, old_motor_Y_value, new_motor_Y_value])
    print("++++++++++++==============)===>", data)
    # data_json = json.dumps(data)
    # print("++++++++++++=================>", data_json)
    context = {
        'compatible_savings_data': [
            {
                'operating_hours_year': compatible_savings_data.get().operating_hours_year,  #
                'operating_hours_day': compatible_savings_data.get().operating_hours_day,  #
                'operating_days_year': compatible_savings_data.get().operating_days_year,  #
                'energy_price': compatible_savings_data.get().energy_price,  #
                'old_motor_energy_year': compatible_savings_data.get().old_motor_energy_year,  #
                'live_time_years': compatible_savings_data.get().live_time_years,  #
                'live_time_old_motor_energy': compatible_savings_data.get().live_time_old_motor_energy,  #
                'live_time_old_motor_energy_price': compatible_savings_data.get().live_time_old_motor_energy_price,  #
                'old_motor_energy_price_year': compatible_savings_data.get().old_motor_energy_price_year,  #
                'old_motor_maintenance_price': compatible_savings_data.get().old_motor_maintenance,  #
                'old_motor_total_costs_in_live_time': compatible_savings_data.get().old_motor_total_costs_in_live_time,
                #
                'new_motor_energy_year': compatible_savings_data.get().new_motor_energy_year,  #
                'live_time_new_motor_energy': compatible_savings_data.get().live_time_new_motor_energy,  #
                'new_motor_energy_price_year': compatible_savings_data.get().new_motor_energy_price_year,  #
                'live_time_new_motor_energy_price': compatible_savings_data.get().live_time_new_motor_energy_price,  #
                'new_motor_list_price': compatible_new_motor.get().motor_price,
                'customer_price': compatible_savings_data.get().customer_price,  #
                'price_discount': compatible_savings_data.get().price_discount,  #
                'new_motor_total_costs_in_live_time': compatible_savings_data.get().new_motor_total_costs_in_live_time,
                #
                'roi_time_years': compatible_savings_data.get().roi_time_years,  #
                'saved_money_live_time': compatible_savings_data.get().saved_money_live_time,  #
                'co2_savings_life_time': compatible_savings_data.get().co2_savings_life_time,  #
                'saved_energy_live_time': compatible_savings_data.get().saved_energy_live_time,  #
                'old_motor_id_savings': compatible_savings_data.get().old_motor_id_savings,  # Adaugat ulterior


            },
        ],
        'old_motor_context': old_motor,
        'data_json1': data, # Adaugat ulterior
    }
    return render(request, 'savings_and_commercial_data/read_savings_and_commercial_data.html', context)


def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None

def generate_pdf(request):
    all_savings = show_savings_and_commercial_data_view
    context = {
        'all_savings': all_savings
    }
    pdf_file = render_to_pdf('savings_and_commercial_data/all_savings_pdf.html', context)
    if pdf_file:
        response = HttpResponse(pdf_file, content_type='application/pdf')
    filename = 'Report with list of savings'
    content = "inline; filename=%s" % filename
    response['Content-Disposition'] = content
    return response

@login_required
def generate_pdf_per_motor(request, pk):
    print("******************", pk)
    savings = SavingsAndCommercialData.objects.get(old_motor_id_savings=pk)
    old_motor = OldMotor.objects.get(id=pk)
    new_motor = NewMotor.objects.get(id=pk)
    context = {
        "details_savings": savings,
        "old_motor_savings": old_motor,
        "new_motor_savings": new_motor
    }
    pdf_file = render_to_pdf('savings_and_commercial_data/savings_to_pdf.html', context)
    if pdf_file:
        response = HttpResponse(pdf_file, content_type='application/pdf')
        filename = f'Details about the savings {old_motor.id}{old_motor.motor_producer}'
        content = 'inline; filename=%s' % filename
        response['Content-Disposition'] = content
        return response

# def home(request, pk):
#     savings = SavingsAndCommercialData.objects.get(old_motor_id_savings=pk)
#     x_var = savings.live_time_years     # 'X' end of scale
#     y_var_old_motor_start = savings.old_motor_maintenance    # 'Y-OLD MOTOR' start of scale
#     y_var_old_motor_end = savings.old_motor_total_costs_in_live_time    # 'Y-OLD MOTOR' end of scale
#     y_var_new_motor_start = savings.customer_price  # 'Y-NEW MOTOR' start of scale
#     y_var_new_motor_end = savings.live_time_new_motor_energy_price + savings.customer_price # 'Y-NEW MOTOR' end of scale
#     data = [['Year', 'Old motor', 'New motor']]
#     print("************** y_var_old_motor_end", y_var_old_motor_end)
#     print("************** y_var_new_motor_start", y_var_new_motor_start)
#     print("************** y_var_new_motor_end", y_var_new_motor_end)
#     for i in range(0, x_var+1):
#         old_motor_Y_value = savings.old_motor_energy_year * i + savings.old_motor_maintenance
#         new_motor_Y_value = savings.new_motor_energy_year * i + savings.customer_price
#         data.append([i, old_motor_Y_value, new_motor_Y_value])






