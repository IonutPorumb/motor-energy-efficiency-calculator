
from django.urls import path

from savings_and_commercial_data import views

urlpatterns = [
    # path('savings-and-commercial-data/', views.SavingsAndCommercialDataCreateView.as_view(),
    #      name='savings_and_commercial_data'),
    path('savings-and-commercial-data/<int:pk>/', views.savings_view,
         name='savings_and_commercial_data'),
    path('show-savings-and-commercial-data/<int:pk>/', views.show_savings_and_commercial_data_view,
         name='show_savings_and_commercial_data'),
    path('generate-pdf-savings/<int:pk>/', views.generate_pdf_per_motor, name='generate_pdf_savings')

]
