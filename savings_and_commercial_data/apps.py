from django.apps import AppConfig


class ComercialDataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'savings_and_commercial_data'
