from datetime import datetime
from math import sqrt

from old_motor.models import OldMotor
from savings_and_commercial_data.models import SavingsAndCommercialData


def energy_calculation_kWh_day(motor_current, motor_voltage, motor_power_factor, operating_hours_day):
    electric_power = sqrt(3) * motor_current * motor_voltage * motor_power_factor / 1000
    energy_day = round(electric_power * operating_hours_day, 2)
    return energy_day


def energy_price_calculation(motor_energy, kWh_price):
    energy_price = round(motor_energy * kWh_price, 2)
    return energy_price


def amortization_years(new_motor_energy_year_price, old_motor_energy_year_price, new_motor_price):
    energy_year_difference = old_motor_energy_year_price - new_motor_energy_year_price
    number_of_years = float(new_motor_price / energy_year_difference)
    return number_of_years


def show_savings_and_commercial_data(old_motor_id):

    old_motor = OldMotor.objects.get(id=old_motor_id)
    compatible_savings_data = SavingsAndCommercialData.objects.filter(old_motor_id_savings=old_motor.id)
    print("***********************", compatible_savings_data)
    print("++++++++++++++++++", compatible_savings_data.get().old_motor_id_savings == old_motor_id)
    return compatible_savings_data


if __name__ == "__main__":

    old_motor_energy = energy_calculation_kWh_day(8.2, 230, 0.82, 24)
    new_motor_energy = energy_calculation_kWh_day(7.8, 230, 0.79, 24)

    old_motor_energy_price = energy_price_calculation(old_motor_energy, 0.11)
    new_motor_energy_price = energy_price_calculation(new_motor_energy, 0.11)
    amortization_old_new = amortization_years(new_motor_energy_price, old_motor_energy_price, 310)

    print("Old motor energy kWh:", old_motor_energy, '\n', 'New motor energy kWh:', new_motor_energy)
    print("Old motor energy price EUR:", old_motor_energy_price, '\n',
          'New motor energy price EUR:', new_motor_energy_price)
    print('Amortization number of days:', amortization_old_new)

    print(show_savings_and_commercial_data(10))


