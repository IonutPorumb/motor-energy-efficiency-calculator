from django import forms
from django.forms import TextInput
from savings_and_commercial_data.models import SavingsAndCommercialData


class SavingsAndCommercialDataForm(forms.ModelForm):
    class Meta:
        model = SavingsAndCommercialData
        fields = ['energy_price', 'operating_hours_day', 'operating_days_year', 'live_time_years',
                  'operating_hours_year', 'old_motor_id_savings'
                  ]
        widgets = {
            'energy_price': TextInput(attrs={'placeholder': 'Insert energy price [EUR/kWh]',
                                             'style': 'opacity:70%; width:80%; margin-top:0.2%; margin-left:0.1%'}),
            'operating_hours_day': TextInput(attrs={'placeholder': 'Insert operating hours/day',
                                                    'style': 'opacity:70%; width:80%; margin-left:0.1%'}),
            'operating_days_year': TextInput(attrs={'placeholder': 'Insert operating days/year',
                                                    'style': 'opacity:70%;width:80%; margin-left:0.1%'}),
            'operating_hours_year': TextInput(attrs={'placeholder': 'Insert operating hours/year',
                                                     'style': 'opacity:70%; width:80%'}),
            'live_time_years': TextInput(attrs={'placeholder': 'Insert live time in years',
                                                'style': 'opacity:70%;width:80%; margin-top:0.2%'}),
            'new_motor_energy_year': TextInput(attrs={'class': 'form-control'}),
            'old_motor_energy_year': TextInput(attrs={'class': 'form-control'}),
            'new_motor_energy_price_year': TextInput(
                attrs={'placeholder': 'Insert the energy consumed by the new motor',
                       'class': 'form-control'}),
            'old_motor_energy_price_year': TextInput(attrs={'placeholder': 'Insert the price for energy '
                                                                           'consumed by the old motor',
                                                            'class': 'form-control'}),
            'new_motor_total_costs_in_live_time': TextInput(
                attrs={'placeholder': 'Insert the energy consumed by the old motor',
                       'class': 'form-control'}),
            'old_motor_total_costs_in_live_time': TextInput(
                attrs={'placeholder': 'Insert the energy consumed by the old motor',
                       'class': 'form-control'}),
            'live_time_new_motor_energy_price': TextInput(
                attrs={'placeholder': 'Insert the energy consumed by the old motor',
                       'class': 'form-control'}),
            'live_time_old_motor_energy_price': TextInput(
                attrs={'placeholder': 'Insert the energy consumed by the old motor',
                       'class': 'form-control'}),
            'saved_energy_live_time': TextInput(attrs={'placeholder': 'Saved energy in [kWh]'}),
            'saved_money_live_time': TextInput(attrs={'placeholder': 'Saved money in [EUR]'}),
            'roi_time_years': TextInput(attrs={'placeholder': 'Return of investment [Year]'}),
            'customer_price': TextInput(attrs={'placeholder': 'Customer price in [EUR]'}),
            'price_discount': TextInput(attrs={'placeholder': 'Insert the price for energy '
                                                              'consumed by the new motor',
                                               'class': 'form-control'}),
            'co2_savings_life_time': TextInput(attrs={'class': 'form-control'}),
            'old_motor_id_savings': TextInput(attrs={'class': 'form-control'}),




        }

    def __init__(self, *args, **kwargs):
        super(SavingsAndCommercialDataForm, self).__init__(*args, **kwargs)
