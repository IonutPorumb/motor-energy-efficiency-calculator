function dataTypeAlert(){
    const inputData = document.getElementsByClassName('numeric');
    let alertMessage = document.getElementById("alert_message");
    console.log(inputData);

    try {
        for(let i = 0; i<inputData.length; i++){
            let inputNumber = Number(inputData.item(i).value);
            if (Object.is(NaN, inputNumber)){
                console.log("not a Number");
                inputData.item(i).value = '';
                // alert("The inserted value must be numeric!")
                alertMessage.style.visibility='visible';
                break;
            }
        }
    }
    catch (e){
        console.log(e);
    }
}