from django.http import HttpResponse
from django.shortcuts import render
from django.template import context


def home(request):
    return render(request, 'home/home.html')
    # return HttpResponse('Hello world!')
