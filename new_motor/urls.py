from django.urls import path

from new_motor import views

urlpatterns = [
    path('create-new_motor/', views.NewMotorCreateView.as_view(), name='create_new_motor'),
    path('list-of-new-motors/', views.NewMotorListView.as_view(), name='view_list_new_motors'),
    path('update-old-motor/<int:pk>/', views.NewMotorUpdateView.as_view(), name='update_new_motor'),
    path('delete-new-motor/<int:pk>/', views.NewMotorDeleteView.as_view(), name='delete_new_motor'),
    path('detail-new-motor/<int:pk>/', views.NewMotorDetailView.as_view(), name='detail_new_motor'),
    path('show-alternative-new-motor/<int:pk>/', views.show_alternative_new_motors_view,
         name='show_alternative_new_motor'),

]
