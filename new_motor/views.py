from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from new_motor.forms import NewMotorForm

# Introduce registrations to DB
from new_motor.functions import chose_new_motor
from new_motor.models import NewMotor
from old_motor.models import OldMotor


class NewMotorCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'new_motor/create_new_motor.html'
    form_class = NewMotorForm
    # fields = '__all__'
    permission_required = 'new_motor.add_new_motor'
    model = NewMotor
    success_url = reverse_lazy('home-page')


# Show dates registered in DB
class NewMotorListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'new_motor/list_new_motor.html'
    model = NewMotor
    permission_required = 'new_motor.view_list_new_motors'
    context_object_name = 'all_new_motors'


# Update new motor form List
class NewMotorUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'new_motor/update_new_motor.html'
    model = NewMotor
    form_class = NewMotorForm
    permission_required = 'new_motor.change_new_motor'
    success_url = reverse_lazy('view_list_new_motors')


class NewMotorDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'new_motor/delete_new_motor.html'
    model = NewMotor
    permission_required = 'new_motor.delete_new_motor'
    success_url = reverse_lazy('view_list_new_motors')
    context_object_name = 'new_motor'


class NewMotorDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'new_motor/detail_new_motor.html'
    model = NewMotor
    permission_required = 'new_motor.view_new_motor'
    context_object_name = 'new_motor'


@login_required
def show_alternative_new_motors_view(request, pk):
    compatible_motors = chose_new_motor(pk)
    motor_frequency_50 = 50
    motor_frequency_60 = 60
    context = {
        'compatible_motors': [
            {
                'motor_frequency_50': motor_frequency_50,
                'motor_frequency_60': motor_frequency_60,
                'old_motor_app_name': OldMotor.objects.get(id=pk).application_name,
                'frame_size_comp': compatible_motors.get().frame_size,
                'rated_power_50Hz': compatible_motors.get().rated_power_50Hz,
                'rated_power_60Hz': compatible_motors.get().rated_power_60Hz,
                'rated_voltage_50Hz_D': compatible_motors.get().rated_voltage_50Hz_D,
                'rated_voltage_50Hz_Y': compatible_motors.get().rated_voltage_50Hz_Y,
                'rated_voltage_60Hz': compatible_motors.get().rated_voltage_60Hz,
                'type_of_construction': compatible_motors.get().type_of_construction,
                'rated_speed_50Hz': compatible_motors.get().rated_speed_50Hz,
                'rated_speed_60Hz': compatible_motors.get().rated_speed_60Hz,
                'rated_torque_50Hz': compatible_motors.get().rated_torque_50Hz,
                'rated_efficiency_50Hz_4_4': compatible_motors.get().rated_efficiency_50Hz_4_4,
                'power_factor_50Hz_4_4': compatible_motors.get().power_factor_50Hz_4_4,
                'power_factor_60Hz_4_4': compatible_motors.get().power_factor_60Hz_4_4,
                'rated_current_50Hz_D': compatible_motors.get().rated_current_50Hz_D,
                'rated_current_50Hz_Y': compatible_motors.get().rated_current_50Hz_Y,
                'rated_current_60Hz': compatible_motors.get().rated_current_60Hz,
                'article_no': compatible_motors.get().article_no,
                'weight': compatible_motors.get().weight,
                'degree_of_protection': compatible_motors.get().degree_of_protection,
                'enclosure_type': compatible_motors.get().enclosure_type,
                'producer': compatible_motors.get().producer,
                'ie_class': compatible_motors.get().ie_class,

            },
        ]
    }
    return render(request, 'new_motor/new_motor_home.html', context)



