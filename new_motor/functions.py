
from new_motor.models import NewMotor
from old_motor.models import OldMotor


def chose_new_motor(id_old_motor):

    old_motor = OldMotor.objects.get(id=id_old_motor)

    compatible_motors = NewMotor.objects.filter(rated_power_50Hz=old_motor.rated_power_D1,
                                                frame_size=old_motor.frame_size,
                                                type_of_construction=old_motor.type_of_construction,
                                                rated_voltage_50Hz_Y=old_motor.rated_voltageY,
                                                rated_voltage_50Hz_D__lte=old_motor.rated_voltageD1,
                                                pole_no=old_motor.pole_no,


                                                )
    return compatible_motors
