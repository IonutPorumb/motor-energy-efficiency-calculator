
import sqlite3

import xlrd

new_motors = xlrd.open_workbook('../new_motor/xls_documents/Lista Motoare_Siemens_1LE_Rev6.xls')
sheet = new_motors.sheet_by_name('motor_list')

data_base = sqlite3.connect('../db.sqlite3')
cursor = data_base.cursor()

for row in range(1, sheet.nrows):
    rated_power_50Hz = round(sheet.cell(row, 0).value, 2)
    rated_power_60Hz = round(sheet.cell(row, 1).value, 2)
    rated_voltage_50Hz_D = sheet.cell(row, 2).value
    rated_voltage_50Hz_Y = sheet.cell(row, 3).value
    rated_voltage_60Hz = sheet.cell(row, 4).value
    type_of_construction = sheet.cell(row, 5).value
    motor_protection = sheet.cell(row, 6).value
    terminal_box_position = sheet.cell(row, 7).value
    frame_size = sheet.cell(row, 8).value
    rated_speed_50Hz = sheet.cell(row, 9).value
    rated_speed_60Hz = sheet.cell(row, 10).value
    pole_no = sheet.cell(row, 11).value
    degree_of_protection = sheet.cell(row, 12).value
    ie_class = sheet.cell(row, 13).value
    enclosure_type = sheet.cell(row, 14).value
    producer = sheet.cell(row, 15).value
    rated_torque_50Hz = round(sheet.cell(row, 16).value, 2)
    rated_efficiency_50Hz_4_4 = round(sheet.cell(row, 17).value, 2)
    rated_efficiency_50Hz_3_4 = round(sheet.cell(row, 18).value, 2)
    rated_efficiency_50Hz_2_4 = round(sheet.cell(row, 19).value, 2)
    power_factor_50Hz_4_4 = round(sheet.cell(row, 20).value, 2)
    power_factor_60Hz_4_4 = round(sheet.cell(row, 21).value, 2)
    rated_current_50Hz_D = round(sheet.cell(row, 22).value, 2)
    rated_current_50Hz_Y = sheet.cell(row, 23).value
    rated_current_60Hz = round(sheet.cell(row, 24).value, 2)
    locked_rotor_torque_50Hz = round(sheet.cell(row, 25).value, 2)
    locked_rotor_current_50Hz = round(sheet.cell(row, 26).value, 2)
    brake_down_torque_50Hz = round(sheet.cell(row, 27).value, 2)
    surface_sound_pressure_50Hz = sheet.cell(row, 28).value
    sound_power_50Hz = sheet.cell(row, 29).value
    article_no = sheet.cell(row, 30).value
    weight = sheet.cell(row, 31).value
    moment_of_inertia = round(sheet.cell(row, 32).value, 4)
    motor_price = round(sheet.cell(row, 33).value, 2)
    print(article_no, " with the price", motor_price)

    query = f"INSERT INTO new_motor_newmotor(rated_power_50Hz, rated_power_60Hz, rated_voltage_50Hz_D, " \
            f"rated_voltage_50Hz_Y, rated_voltage_60Hz, type_of_construction, motor_protection," \
            f"terminal_box_position, frame_size, rated_speed_50Hz, rated_speed_60Hz, pole_no, " \
            f"degree_of_protection, ie_class, enclosure_type, producer, rated_torque_50Hz, " \
            f"rated_efficiency_50Hz_4_4, rated_efficiency_50Hz_3_4, rated_efficiency_50Hz_2_4," \
            f"power_factor_50Hz_4_4, power_factor_60Hz_4_4, rated_current_50Hz_D, rated_current_50Hz_Y," \
            f"rated_current_60Hz, locked_rotor_torque_50Hz, locked_rotor_current_50Hz, brake_down_torque_50Hz, " \
            f"surface_sound_pressure_50Hz, sound_power_50Hz, article_no, weight, moment_of_inertia, motor_price)" \
            f"VALUES ('{rated_power_50Hz}', '{rated_power_60Hz}', '{rated_voltage_50Hz_D}', " \
            f"'{rated_voltage_50Hz_Y}', '{rated_voltage_60Hz}', '{type_of_construction}', '{motor_protection}'," \
            f"'{terminal_box_position}', '{frame_size}', '{rated_speed_50Hz}', '{rated_speed_60Hz}'," \
            f"'{pole_no}', '{degree_of_protection}', '{ie_class}', '{enclosure_type}', '{producer}'," \
            f"'{rated_torque_50Hz}', '{rated_efficiency_50Hz_4_4}', '{rated_efficiency_50Hz_3_4}',"\
            f"'{rated_efficiency_50Hz_2_4}','{power_factor_50Hz_4_4}', '{power_factor_60Hz_4_4}', " \
            f"'{rated_current_50Hz_D}', '{rated_current_50Hz_Y}', '{rated_current_60Hz}', " \
            f"'{locked_rotor_torque_50Hz}', '{locked_rotor_current_50Hz}', '{brake_down_torque_50Hz}'," \
            f"'{surface_sound_pressure_50Hz}', '{sound_power_50Hz}', '{article_no}', '{weight}'," \
            f"'{moment_of_inertia}', '{motor_price}')"

    cursor.execute(query)
cursor.close()
data_base.commit()
data_base.close()
