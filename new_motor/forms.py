from django import forms
from django.forms import TextInput, Select

from new_motor.models import NewMotor


class NewMotorForm(forms.ModelForm):
    class Meta:
        model = NewMotor
        fields = ['rated_power_50Hz', 'rated_power_60Hz', 'rated_voltage_50Hz_D', 'rated_voltage_50Hz_Y',
                  'rated_voltage_60Hz', 'type_of_construction', 'motor_protection',
                  'terminal_box_position', 'frame_size', 'rated_speed_50Hz', 'rated_speed_60Hz', 'pole_no',
                  'degree_of_protection', 'ie_class', 'enclosure_type', 'producer', 'rated_torque_50Hz',
                  'rated_efficiency_50Hz_4_4', 'rated_efficiency_50Hz_3_4', 'rated_efficiency_50Hz_2_4',
                  'power_factor_50Hz_4_4', 'power_factor_60Hz_4_4', 'rated_current_50Hz_D', 'rated_current_50Hz_Y',
                  'rated_current_60Hz', 'locked_rotor_torque_50Hz', 'locked_rotor_current_50Hz',
                  'brake_down_torque_50Hz', 'surface_sound_pressure_50Hz', 'sound_power_50Hz', 'article_no', 'weight',
                  'moment_of_inertia', 'motor_price']
        widgets = {
            'rated_power_50Hz': TextInput(attrs={'placeholder': 'Insert the rated power at 50Hz in [kW]',
                                                 'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_power_60Hz': TextInput(attrs={'placeholder': 'Insert the rated power at 60Hz in [kW]',
                                                 'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_voltage_50Hz_D': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_voltage_50Hz_Y': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_voltage_60Hz': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'type_of_construction': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'motor_protection': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'terminal_box_position': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'frame_size': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_speed_50Hz': TextInput(attrs={'placeholder': 'Insert the rated speed at 50Hz in [rpm]',
                                                 'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_speed_60Hz': TextInput(attrs={'placeholder': 'Insert the rated speed at 60Hz in [rpm]',
                                                 'class': 'form-control', 'style': 'opacity:70%;'}),
            'pole_no': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'degree_of_protection': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'ie_class': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'enclosure_type': Select(attrs={'class': 'form-control', 'style': 'opacity:70%;'}),
            'producer': TextInput(attrs={'placeholder': 'Insert the motor producer name', 'class': 'form-control'}),
            'rated_torque_50Hz': TextInput(attrs={'placeholder': 'Insert the rated torque at 50Hz in [Nm]',
                                                  'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_efficiency_50Hz_4_4': TextInput(
                attrs={'placeholder': 'Insert the rated efficiency at 50Hz full load',
                       'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_efficiency_50Hz_3_4': TextInput(
                attrs={'placeholder': 'Insert the rated efficiency at 50Hz 3/4 load',
                       'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_efficiency_50Hz_2_4': TextInput(
                attrs={'placeholder': 'Insert the rated efficiency at 50Hz 2/4 load',
                       'class': 'form-control', 'style': 'opacity:70%;'}),
            'power_factor_50Hz_4_4': TextInput(attrs={'placeholder': 'Insert the power factor value for 50Hz',
                                                      'class': 'form-control', 'style': 'opacity:70%;'}),
            'power_factor_60Hz_4_4': TextInput(attrs={'placeholder': 'Insert the power factor value for 60Hz',
                                                      'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_current_50Hz_D': TextInput(attrs={'placeholder': 'Insert the rated current for 50Hz, D '
                                                                    'connection value in [A]',
                                                     'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_current_50Hz_Y': TextInput(attrs={'placeholder': 'Insert the rated current for 50Hz, Y '
                                                                    'connection value in [A]',
                                                     'class': 'form-control', 'style': 'opacity:70%;'}),
            'rated_current_60Hz': TextInput(attrs={'placeholder': 'Insert the rated current for 60Hz '
                                                                  'value in [A]', 'class': 'form-control'}),
            'locked_rotor_torque_50Hz': TextInput(attrs={'placeholder': 'Insert the locked rotor torque in [Nm]',
                                                         'class': 'form-control', 'style': 'opacity:70%;'}),
            'locked_rotor_current_50Hz': TextInput(attrs={'placeholder': 'Insert the locked rotor current in [A]',
                                                          'class': 'form-control', 'style': 'opacity:70%;'}),
            'brake_down_torque_50Hz': TextInput(attrs={'placeholder': 'Insert the brake down torque in [Nm]',
                                                       'class': 'form-control', 'style': 'opacity:70%;'}),
            'surface_sound_pressure_50Hz': TextInput(attrs={'placeholder': 'Insert the surface sound pressure in [dB]',
                                                            'class': 'form-control', 'style': 'opacity:70%;'}),
            'sound_power_50Hz': TextInput(attrs={'placeholder': 'Insert the sound power in [dB]',
                                                 'class': 'form-control', 'style': 'opacity:70%;'}),
            'article_no': TextInput(attrs={'placeholder': 'Insert the article code number', 'class': 'form-control',
                                           'style': 'opacity:70%;'}),
            'weight': TextInput(attrs={'placeholder': 'Insert the new_motor weight in [kg]', 'class': 'form-control',
                                       'style': 'opacity:70%;'}),
            'moment_of_inertia': TextInput(attrs={'placeholder': 'Insert the new motor moment of inertia in [kgm2]',
                                                  'class': 'form-control', 'style': 'opacity:70%;'}),
            'motor_price': TextInput(attrs={'placeholder': 'Insert the new_motor price in EUR',
                                            'class': 'form-control', 'style': 'opacity:70%;'}),
        }

    def __init__(self, *args, **kwargs):
        super(NewMotorForm, self).__init__(*args, **kwargs)
