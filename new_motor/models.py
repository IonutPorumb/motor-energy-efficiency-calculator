from django.db import models


# Create the AcMotor class:


class NewMotor(models.Model):
    voltage_options = ((220, 220),
                       (230, 230),
                       (380, 380),
                       (400, 400),
                       (440, 440),
                       (460, 460),
                       (500, 500),
                       (575, 575),
                       (660, 660),
                       (690, 690))
    construction_options = (('IM B3', 'IM B3'),
                            ('IM B5', 'IM B5'),
                            ('IM B35', 'IM B35'),
                            ('IM B14', 'IM B14'))
    protection_options = (('Without', 'Without'),
                          ('PTC-3 alarm sensors', 'PTC-3 alarm sensors'),
                          ('PTC-3 fault sensors', 'PTC-3 fault sensors'),
                          ('PT100-3 sensors', 'PT100-3 sensors'),
                          ('PT100-6 sensors', 'PT100-6 sensors'))
    terminal_box_position_option = (('Top', 'Top'),
                                    ('Left', 'Left'),
                                    ('Bottom', 'Bottom'),
                                    ('Right', 'Right'))
    frame_size_options = (('71M', '71M'),
                          ('80M', '80M'),
                          ('90S', '90S'), ('90L', '90L'),
                          ('100L', '100L'),
                          ('112M', '112M'),
                          ('132S', '132S'), ('132M', '132M'),
                          ('160M', '160M'), ('160L', '160L'),
                          ('180M', '180M'), ('180L', '180L'),
                          ('200L', '200L'),
                          ('225S', '225S'), ('225M', '225M'),
                          ('250M', '250M'),
                          ('280S', '280S'), ('280M', '280M'),
                          ('315S', '315S'), ('315M', '315M'), ('315L', '315L'))
    pole_options = ((2, 2), (4, 4), (6, 6), (8, 8), (10, 10), (12, 12), (16, 16))
    enclosure_options = (('Aluminium', 'Aluminium'), ('Cast iron', 'Cast iron'))
    degree_of_protection_options = (('IP54', 'IP54'), ('IP55', 'IP55'), ('IP56', 'IP56'),
                                    ('IP65', 'IP65'), ('IP66', 'IP66'))
    ie_class_options = (('IE1', 'IE1'), ('IE2', 'IE2'), ('IE3', 'IE3'), ('IE4', 'IE4'))
    rated_power_50Hz = models.FloatField(null=True)
    # rated_power_50Hz = models.DecimalField(decimal_places=2, max_digits=4, null=True)
    rated_power_60Hz = models.FloatField(null=True)
    rated_voltage_50Hz_D = models.IntegerField(choices=voltage_options, null=True)
    rated_voltage_50Hz_Y = models.IntegerField(choices=voltage_options, null=True)
    rated_voltage_60Hz = models.IntegerField(choices=voltage_options, null=True)
    type_of_construction = models.CharField(max_length=10, choices=construction_options)
    motor_protection = models.CharField(max_length=30, choices=protection_options, default='')
    terminal_box_position = models.CharField(max_length=10, choices=terminal_box_position_option)
    frame_size = models.CharField(max_length=5, choices=frame_size_options)
    rated_speed_50Hz = models.IntegerField(null=True)
    rated_speed_60Hz = models.IntegerField(null=True)
    pole_no = models.IntegerField(null=True, choices=pole_options)
    degree_of_protection = models.CharField(max_length=4, null=True, blank=True,
                                            choices=degree_of_protection_options)
    ie_class = models.CharField(max_length=3, null=True, blank=True, choices=ie_class_options)
    enclosure_type = models.CharField(max_length=10, null=True, blank=True,
                                      choices=enclosure_options)
    producer = models.CharField(max_length=100, null=True, blank=True)
    rated_torque_50Hz = models.FloatField(null=True)
    rated_efficiency_50Hz_4_4 = models.FloatField(null=True)
    rated_efficiency_50Hz_3_4 = models.FloatField(null=True)
    rated_efficiency_50Hz_2_4 = models.FloatField(null=True)
    power_factor_50Hz_4_4 = models.FloatField(null=True)
    power_factor_60Hz_4_4 = models.FloatField(null=True)
    rated_current_50Hz_D = models.FloatField(null=True)
    rated_current_50Hz_Y = models.FloatField(null=True)
    rated_current_60Hz = models.FloatField(null=True)
    locked_rotor_torque_50Hz = models.FloatField(null=True)
    locked_rotor_current_50Hz = models.FloatField(null=True)
    brake_down_torque_50Hz = models.FloatField(null=True)
    surface_sound_pressure_50Hz = models.IntegerField(null=True)
    sound_power_50Hz = models.IntegerField(null=True)
    article_no = models.CharField(max_length=18, null=True)
    weight = models.FloatField(null=True)
    moment_of_inertia = models.FloatField(null=True)
    motor_price = models.FloatField(null=True)

    def __str__(self):
        return f"Order no.:{self.article_no}, power:{self.rated_power_50Hz}" \
               f"Speed:{self.rated_speed_50Hz}, Motor current:{self.rated_current_50Hz_D}" \
               f"Type of construction: {self.type_of_construction} \n"
