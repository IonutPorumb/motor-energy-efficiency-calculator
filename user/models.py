from django.contrib.auth.models import User
from django.db import models


class ExtendUser(User):
    sex_options = (('Male', 'male'), ('Female', 'female'))
    date_of_birth = models.DateField(null=True)
    sex = models.CharField(max_length=6, choices=sex_options)
    company_name = models.CharField(max_length=150, null=True)
    occupation = models.CharField(max_length=150, null=True)

    def __str__(self):
        return f"First name: {self.first_name}, \n" \
               f"Last name: {self.last_name} \n" \
               f"Date of birth: {self.date_of_birth} \n" \
               f"Company name: {self.company_name} \n" \
               f"Occupation: {self.occupation} \n"
