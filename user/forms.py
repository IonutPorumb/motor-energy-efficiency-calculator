from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput, Select

from user.models import ExtendUser


class UserForms(UserCreationForm):
    class Meta:
        model = ExtendUser
        fields = ['first_name', 'last_name', 'email', 'username', 'sex', 'date_of_birth', 'company_name', 'occupation']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please insert your first name',
                                           'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please insert your last name',
                                          'class': 'form-control'}),

            'email': TextInput(attrs={'placeholder': 'Please insert your e-mail address',
                                      'class': 'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Please insert your user name', 'class': 'form-control'}),
            'sex': Select(attrs={'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'company_name': TextInput(attrs={'placeholder': 'Please insert your company name',
                                             'class': 'form-control'}),
            'occupation': TextInput(attrs={'placeholder': 'Please insert your occupation',
                                           'class': 'form-control'})

        }

    def __init__(self, *args, **kwargs):
        super(UserForms, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'FormControl'
        self.fields['password1'].widget.attrs['placeholder'] = 'Please insert a password'
        self.fields['password2'].widget.attrs['class'] = 'FormControl'
        self.fields['password2'].widget.attrs['placeholder'] = 'Please confirm the password'
